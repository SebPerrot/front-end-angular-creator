#!/bin/bash
rootPath=$1
projectName=$2
localPort=$3
remotePort=$4
version=$5
nodeVersion=$6
backendServiceName=$7
backendServiceHost=$8
rootPathname='client'

initial="${rootPath:0:1}"
if [ $initial != "/" ]; then
  rootPath="$PWD/$rootPath"
fi

mkdir $rootPath

docker_compose_file_name="docker-compose.${projectName}-client.yml"

cd $rootPath
mkdir $rootPathname
mkdir docker

cd docker
cat > ${docker_compose_file_name} << EOF 
version: '3.7'

services:

  $projectName-client:
    build:
      context: ../$rootPathname
      dockerfile: Dockerfile
    container_name: "client-$projectName"
    volumes:
      - ./../$rootPathname:/usr/src/app:cached
      - ./../$rootPathname/node_modules:/usr/src/app/node_modules
    ports:
      - $localPort:$remotePort
networks:
  default:
EOF

cd $rootPath/$rootPathname
mkdir node_modules

cat > Dockerfile << EOF
# base image
FROM node:$nodeVersion

WORKDIR /usr/src/

RUN npm i npm@latest -g
RUN npm install -g @angular/cli@latest

CMD ng new $projectName --routing=true --skipGit=true --style=scss --directory=./app
EOF


cat > .dockerignore << EOF
node_modules
.git
EOF

cd $rootPath/docker
dc="docker-compose -f ${docker_compose_file_name}"

$dc build --no-cache
$dc up

cd $rootPath/$rootPathname
rm -rf Dockerfile

cat > Dockerfile << EOF
FROM node:$nodeVersion

WORKDIR /usr/src/app

RUN npm i npm@latest -g

COPY ./package.json /usr/src/tmp/
COPY ./package-lock.json /usr/src/tmp/

RUN npm install

CMD npm start -- --host 0.0.0.0 --watch --proxy-config proxy.conf.json
EOF




cd $rootPath/docker

$dc build --no-cache

$dc up -d
sleep 5
# $dc logs -f

mkdir $rootPath/$rootPathname/src/app/components
mkdir $rootPath/$rootPathname/src/app/pages
mkdir $rootPath/$rootPathname/src/app/services

if [ -z "$backendServiceName" ]
then
      echo "No backend linked"
else
      cat > $rootPath/$rootPathname/proxy.conf.json << EOF
{
    "/api/*": {
        "target": "http://$backendServiceName:$backendServiceHost",
        "secure": false,
        "logLevel": "debug",
        "changeOrigin": true
    }
}
EOF
fi

exit